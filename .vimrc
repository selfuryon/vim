 set nocompatible               " be iMproved
 filetype off                   " required!

 set rtp+=~/.vim/bundle/vundle/
 call vundle#rc()
 
 " let Vundle manage Vundle
 " required! 
 Bundle 'gmarik/vundle'

 " My Bundles here:
 "
 " original repos on ithub
 Bundle 'tpope/vim-fugitive'
 Bundle 'Lokaltog/vim-easymotion'
 Bundle 'rstacruz/sparkup', {'rtp': 'vim/'}
 Bundle 'tpope/vim-rails.git'
 " vim-scripts repos
 Bundle 'L9'
 Bundle 'FuzzyFinder'
 " non github repos
 Bundle 'git://git.wincent.com/command-t.git'
 "My paplugins
 Bundle 'scrooloose/nerdtree'
 Bundle 'taglist.vim'
 Bundle 'xml.vim'
 Bundle 'https://github.com/rosenfeld/conque-term.git'
 Bundle 'surround.vim'
 Bundle 'matchit.zip'
 Bundle 'scrooloose/syntastic'
 Bundle 'kien/tabman.vim'
 Bundle 'snipMate'


 filetype on
 filetype plugin indent on     " required!
 "
 " Brief help
 " :BundleList          - list configured bundles
 " :BundleInstall(!)    - install(update) bundles
 " :BundleSearch(!) foo - search(or refresh cache first) for foo
 " :BundleClean(!)      - confirm(or auto-approve) removal of unused bundles
 "
 " see :h vundle for more details or wiki for FAQ
 " NOTE: comments after Bundle command are not allowed..

"Common Configuration
set tabstop=4
colo delek
syntax on
set number

map <Leader>f <Esc>:%!xmllint --format -<cr> 
map <Leader>v <Esc>:%w !xmllint --valid --noout -<cr>
map <Leader>P <Esc>:set paste<cr>
map <Leader>p <Esc>:set nopaste<cr>

map <F5> <Esc>:ls<cr>
map <F6> <Esc>:bp<cr>
map <F7> <Esc>:bn<cr>

"NERDTree Configuration
map <C-l> :NERDTreeToggle<CR> 
let NERDTreeShowHidden=1
let g:NERDTreeWinPos = "right"

